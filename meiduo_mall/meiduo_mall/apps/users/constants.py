# 定义验证邮件有效期 单位是秒
VERIFY_EMAIL_TOKEN_EXPIRES = 60 * 60 * 24

# 定义验证邮件域名前缀
VERIFY_EMAIL_PREFIX = 'http://www.meiduo.site/success_verify_email.html?token='

# 定义用户最大保存地址数
USER_ADDRESS_COUNTS_LIMIT = 20

# 定义用户最大保存浏览记录数
USER_BROWSING_HISTORY_COUNTS_LIMIT = 20

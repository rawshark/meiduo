# 定义redis时间常量 单位是秒
SMS_CODE_REDIS_EXPIRES = 300

# 发送短信时间间隔  单位是秒
SEND_SMS_CODE_INTERVAL = 60

# 模板编号
SEND_SMS_TEMPLATE_ID = 1
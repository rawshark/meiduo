from drf_haystack.serializers import HaystackSerializer
from rest_framework import serializers

from goods import models
from goods.search_indexes import SKUIndex


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GoodsChannel
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GoodsCategory
        fields = '__all__'


class SKUSerializer(serializers.ModelSerializer):
    """
    SKU序列化器
    """
    class Meta:
        model = models.SKU
        fields = ('id', 'name', 'price', 'default_image_url', 'comments')


class SKUIndexSerializer(HaystackSerializer):
    """
    SKU索引结果数据序列化器
    """
    object = SKUSerializer(read_only=True)

    class Meta:
        index_classes = [SKUIndex]
        fields = ('text', 'object')

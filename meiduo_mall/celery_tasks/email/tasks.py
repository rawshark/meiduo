from celery_tasks.main import app
from django.conf import settings
from django.core.mail import send_mail


@app.task(name='send_verify_email')
def send_verify_email(to_email, verify_url):
    """
    发送验证邮箱邮件
    :param to_email: 收件人邮箱
    :param verify_url: 验证链接
    :return:  None
    """
    subject = "美多商城邮箱验证"
    html_message = '<p>验证邮箱测试！</p>' \
                   '<p>邮箱为：%s 。激活链接：</p>' \
                   '<p><a href="%s">%s<a></p>' % (to_email, verify_url, verify_url)
    send_mail(subject, "", settings.EMAIL_FROM, [to_email], html_message=html_message)
